import { notify } from '../helpers'
import print from 'print-js'

import Vue from 'vue'
const pdf = async (url, _print = false) => {
  try {
    console.log('url', url)
    const resp = await Vue.prototype.$axios.get('/pdf', { params: { url, name: 'pedro-doc' } })
    const data = resp.data
    const status = resp.status
    console.log('data', data, 'url', data.url, data.status)
    if (data.error) notify({ message: 'error ' + data.error.name, error: true, timeout: 4000 })
    else if (status === 200) {
      if (_print) {
        print(data.url)
      } else {
        window.open(data.url, '_blank')
      }
    }
  } catch (error) {
    console.log('error', error.error.name)
    notify({ message: 'error ' + error.error.name, error: true, timeout: 4000 })
    // const error = handleAxiosError(_error)
    // alert(error.statusMsg)
    // if (error.status === 401) root.$router.push('/printDoc')
  }
}
export default pdf
