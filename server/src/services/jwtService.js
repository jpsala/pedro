import jwt from 'jsonwebtoken';
import dotenv from 'dotenv';

dotenv.config();

const tokenKey = process.env.JWT_SECRET;
const getNewToken = (payload, expiresIn = process.env.TOKEN_EXPIRATION_TIME) => {
  return jwt.sign(payload, tokenKey, {
    expiresIn: `${expiresIn}s`, // expires in 24 hours
  });
};
const verifyToken = (token) => jwt.verify(token, tokenKey, (_error, decodedToken) => {
  let error;
  if (_error) {
    error = {
      message: (_error && _error.message === 'jwt expired') ? 'La sesión expiró\nVuelva a ingresar' : _error.message,
    };
  }
  return { decodedToken, error };
});
export { getNewToken, verifyToken };
