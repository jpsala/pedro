/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import db from '../db';
import dayjs from './dayjs';

const getDoc = async (id) => {
  const [rows] = await db().query(`
    SELECT *
      from doc where id = ${id}
  `);
  return rows.length > 0 ? rows[0] : {};
};

const getDocs = async () => {
  const [rows] = await db().query(`
    SELECT *
      from doc
  `);
  return rows;
};
const newDoc = async (doc, userId) => {
  /*
  fecha: "2019-11-24T03:00:00.000Z"
  fecha_creacion: "2019-11-24T03:00:00.000Z"
  fecha_modificacion: null
  id: 4
  numero: 1
  proveedor_id: 1
  retencion: "1.00"
  tipo: "op"
  total: "100.00"
  user_id: 2
*/
  let resp;
  if (doc.id) {
    const fecha = dayjs(doc.fecha, 'DD/MM/YYYY').format('YYYY/MM/DD');
    const fecha_creacion = dayjs(doc.fecha_creacion).format('YYYY/MM/DD');
    // const fecha = `${fecha.getFullYear()}/${fecha.getMonth()}/${fecha.getDate()}}`;
    const [rows] = await db().query(`
      update doc set tipo = '${doc.tipo}', numero = ${doc.numero},
                  proveedor_id = ${doc.proveedor_id}, fecha_creacion = '${fecha_creacion}',
                  fecha = '${fecha}', total = ${doc.total}, user_id = ${doc.user_id},
                  retencion = ${doc.retencion} where id = ${doc.id}

    `);
    // eslint-disable-next-line prefer-destructuring
    // resp = Object.values(rows)[3].split(' ')[5];
    resp = rows;
  } else {
    const fecha = dayjs(doc.fecha, 'DD/MM/YYYY').format('YYYY/MM/DD');

    // const fecha = `${fecha.getFullYear()}/${fecha.getMonth()}/${fecha.getDate()}}`;
    const [rows] = await db().query(`
      INSERT INTO doc(tipo, numero, proveedor_id, fecha,
                  total, user_id, retencion) VALUES
      ('op', ${doc.numero}, ${doc.proveedor_id}, '${fecha}', ${doc.total}, ${userId}, ${doc.retencion});

    `);
    resp = rows;
  }
  return resp;
};

export { getDoc, getDocs, newDoc };
