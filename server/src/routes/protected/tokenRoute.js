import express from 'express';
import { getNewToken, verifyToken } from '../../services/jwtService';

const router = express.Router();
router.get('/', async (req, res) => {
  console.log('req', req)
  const { token } = req.headers;
  const { decodedToken, error } = verifyToken(token);
  if (error) {
    console.log(`checkToken: ${error.message}`, req.baseUrl);
    return res.status(401).send({ auth: false, message: error.message });
  }
  const newToken = getNewToken({ user: decodedToken.user });
  res.status(200).json(newToken);
});

module.exports = router;
