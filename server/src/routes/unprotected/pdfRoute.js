/* eslint-disable no-unused-expressions */
/* eslint-disable no-undef */
/* eslint-disable no-use-before-define */
import express from 'express';
import makeDir from 'make-dir';
import os from 'os';
import { getNewToken } from '../../services/jwtService';

const dotenv = require('dotenv');

const puppeteer = require('puppeteer');
const path = require('path');

const CLIENTPORT = process.env.EXPRESS_PORT;
const router = express.Router();
const production = process.env.NODE_ENV === 'production';
const hostname = os.hostname();

dotenv.config();

router.get('/', async (req, res) => {
  const { url, name } = req.query;

  const browser = await puppeteer.launch({
    args: ['--no-sandbox', '--disable-setuid-sandbox'],
    headless: true, // print to pdf only works in headless mode currently
    userDataDir: './user-data-dir',
  });

  await setToken(browser);

  let page;
  let pageUrl;
  if (hostname === 'iae') {
    pageUrl = `https://hotel.iae.com.ar:3000${url}`;
    console.log('pargeUrl', pageUrl);
  } else {
    pageUrl = (production ? `http://localhost:${CLIENTPORT}/${url}` : `http://localhost:8080/${url}`).replace(/\/\//g, '/');
  }
  try {
    page = await browser.newPage();
    console.log('goto', pageUrl);
    page.goto(pageUrl);
    console.log('wait');
  } catch (error) {
    await browser.close();
    console.log('pdfRoute error, in browser close', error);
    res.status(501).json(error);
    return;
  }
  try {
    await page.waitForSelector('#loaded', {
      timeout: 5000,
    });
  } catch (error) {
    console.log('pdfRoute error in waitForSelector #loaded', error);
    await browser.close();
    console.log('browser close');
    res.status(501).json(error);
    return;
  }

  const pdfName = `${name ? name.replace('.pdf', '') : 'output'}.pdf`;
  process.env.NODE_ENV === 'development' && console.log('pdfName', pdfName);
  const destPath = path.join(__dirname, '../../../../client/dist/spa/pdf/');
  const fileName = path.join(destPath, pdfName);
  process.env.NODE_ENV === 'development' && console.log('saving pdf in', fileName);
  await page.pdf({
    path: fileName,
    format: 'A4',
    landscape: true,
    printBackground: true,
    margin: { top: '20', right: '20', bottom: '20', left: '0' },
  });
  await browser.close();
  console.log('browser close');
  let responseUrl;
  if (hostname === 'iae') responseUrl = `https://hotel.iae.com.ar:3000/pdf/${pdfName}`;
  else responseUrl = `http://localhost:${CLIENTPORT}/pdf/${pdfName}`;

  res.status(200).json({
    url: responseUrl,
  });
});

async function setToken(browser) {
  dotenv.config();
  const page = await browser.newPage();
  const token = getNewToken({ user: { id: -1, nombre: 'imprimiendo' } }, 60);
  if (hostname === 'iae') {
    await page.goto('https://hotel.iae.com.ar:3000');
  } else {
    await page.goto(production ? `http://localhost:${CLIENTPORT}` : 'http://localhost:8080');
  }
  process.env.NODE_ENV === 'development' && console.log(token.substring(-20, 10));
  const respEvaluate = await page.evaluate((_token) => {
    const res = localStorage.setItem('pedro', _token);
    return Promise.resolve({ res, _token });
  }, token);
  process.env.NODE_ENV === 'development' && console.log('pageEvaluate', respEvaluate);
  await page.close();
}
module.exports = router;
