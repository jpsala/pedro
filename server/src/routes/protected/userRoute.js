import express from 'express';
import { getUsers } from '../../services/userService';

const router = express.Router();

router.get('/:id?', async (req, res) => {
  const { id } = req.params;
  const users = await getUsers(id);
  res.status(200).json(users);
});

module.exports = router;
