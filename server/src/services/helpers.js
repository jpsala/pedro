const wait = async (time = 1000) => new Promise((resolve) => {
  setTimeout(() => {
    resolve();
  }, time);
});
const setheaders = (app) => {
  app.use('*', (res, next) => {
    res.setHeader('Access-Control-Expose-Headers', 'token');
    res.setHeader('Access-Control-Allow-Origin', '*');
    next();
  });
};
function strPad(pad, str, pos) {
  if (typeof str === 'undefined') { return pad; }
  if (pos === 'l') {
    return (pad + str).slice(-pad.length);
  }

  return (str + pad).substring(0, pad.length);
}
export { setheaders, wait, strPad };
