const parseToken = (token) => {
  if (!token) return
  try {
    var base64Url = token.split('.')[1]
    var base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    var jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)
    }).join(''))
    return JSON.parse(jsonPayload)
  } catch (error) {
    console.error('parseToken token inválido o no hay token', token)
    return undefined
  }
}
const verifyToken = (token) => {
  let parsedToken
  let seconds
  let error
  try {
    const parsedToken = parseToken(token)
    seconds = Math.floor(parsedToken.exp - (new Date().getTime() + 1) / 1000)
    if (seconds <= 0) {
      error = 'Token expired'
    }
  } catch (_error) {
    error = _error.message
  }
  return { error, parsedToken, seconds }
}
export { parseToken, verifyToken }
