import Vue from 'vue'
import VueRouter from 'vue-router'
import VueCompositionApi from '@vue/composition-api'
import routes from './routes'
Vue.use(VueCompositionApi)
Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation
 */

export default function ({ store }) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as is and change from quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeResolve(async (to, from, next) => {
    let loggedIn = store.getters['session/loggedIn']
    if (!loggedIn && from.path === '/login') return
    if (!loggedIn && to.path !== '/login') {
      Router.push('/login')
      return
    }
    next()
  })
  return Router
}
