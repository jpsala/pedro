import Axios from 'axios'
import { getProveedores } from '../services/proveedor'
import { Dialog } from 'quasar'
import Vue from 'vue'
const axiosIae = Axios.create({
  baseURL: 'http://iae.dyndns.org/iae/index.php?r=apiApp/'
})
const retenciones = []
const getRetencion = async (proveedor) => {
  const { id, nombre, cuit } = proveedor
  const retencion = await axiosIae.post('getAlicuota', { cuit: cuit.replace(/-/g, '') })
  retenciones.push({ id, nombre, cuit, retencion: retencion.data })
}
const getRetencionesDelIAE = async () => {
  const proveedores = await getProveedores()
  await proveedores.reduce(async (accumulatorPromise, proveedor) => {
    await accumulatorPromise
    return getRetencion(proveedor)
  }, Promise.resolve())
  return retenciones
}
const setRetenciones = async (retenciones) => {
  return Vue.prototype.$axios.post('retenciones', retenciones)
}
const getRetenciones = async (desde, hasta) => {
  const resp = await Vue.prototype.$axios.get('retenciones', { params: { desde, hasta } })
  return resp.data
}
const traeArchivo = async (desde, hasta) => {
  try {
    const resp = await Vue.prototype.$axios.get('retenciones/archivo', { params: { desde, hasta } })
    return resp.data.url
  } catch (error) {
    console.log('catch trae archivo', error)
    Dialog.create({ title: 'Error bajando el archivo', message: error.msg || 'Error desconocido' })
  }
}

export { getRetencionesDelIAE, setRetenciones, getRetenciones, traeArchivo }
