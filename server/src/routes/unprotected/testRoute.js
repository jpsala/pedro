import express from 'express';

const router = express.Router();

// handles url http://localhost:6001/products
router.get('/', async (req, res, next) => {

  res.status(200).json({
    data: 'unprotected Route ',
  });
});


module.exports = router;
