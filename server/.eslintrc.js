module.exports = {
  extends: [
    // add more generic rulesets here, such as:
    // "@vue/airbnb"
    'airbnb-base'
  ],

  rules: {
    'no-param-reassign': [
      'error',
      {
        props: true,
        ignorePropertyModificationsFor: [
          'state',
          'acc',
          'e',
          'ctx',
          'req',
          'request',
          'res',
          'response',
          '$scope',
        ],
      },
    ],
    'import/prefer-default-export': 'off',
    'no-underscore-dangle': 0,
    'no-shadow': ['error', { allow: ['state'] }],
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'object-curly-newline': 'off',
    'class-methods-use-this': ['error', {
      exceptMethods: [
        'store',
        'getInitialState',
        'getDefaultProps',
        'getChildContext',
        'componentWillMount',
        'componentDidMount',
        'componentWillReceiveProps',
        'shouldComponentUpdate',
        'componentWillUpdate',
        'componentDidUpdate',
        'componentWillUnmount',
      ],
    }],
    // 'no-shadow' : ["error", { "allow": ["state"] }]
  },

  root: true,

  env: {
    node: true,
  },

  parserOptions: {
    parser: 'babel-eslint',
  },
};
