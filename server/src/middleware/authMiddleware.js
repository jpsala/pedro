/* eslint-disable no-unused-expressions */
import { verifyToken, getNewToken } from '../services/jwtService';

export function checkToken(req, res, next) {
  console.log('req', req.method, req.baseUrl);
  const { token } = req.headers;
  process.env.NODE_ENV === 'development' && console.log('auth middleware token', token.substring(-20, 10));
  const { decodedToken, error } = verifyToken(token);

  if (error) {
    console.log(`checkToken: ${error.message}`, req.baseUrl);
    return res.status(401).send({ auth: false, message: error.message });
  }
  const seconds = Math.floor(decodedToken.exp - (new Date().getTime() + 1) / 1000);
  if (seconds < 60) {
    const newToken = getNewToken({ user: decodedToken.user });
    req.user = decodedToken;
    res.setHeader('token', newToken);
  }
  return next();
}
