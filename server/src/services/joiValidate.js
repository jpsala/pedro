const joiErrors = (errors) => {
  console.log('errors antes', errors);
  errors.forEach((err) => {
    console.log('err en el foreach', err);
    let { message } = err;
    const { label } = err.local;
    switch (err.code) {
      case 'date.format':
        message = `${label} (${err.value}) debe tener formato ${err.local.format}`;
        console.log('err', err);
        break;
      case 'number.greater':
        message = `"${label}" debe ser mayor a ${err.local.limit}`;
        break;
      case 'number.min':
        message = `"${label}" debe ser mayor o igual a ${err.local.limit}`;
        break;
      case 'string.base':
        message = `"${label}" debe ser alfanumérico!`;
        break;
      case 'string.empty':
        message = `"${label}" no puede estar vacio!`;
        break;
      case 'string.min':
        message = `${label} no puede tener menos de ${err.local.limit} caracteres!`;
        break;
      case 'string.max':
        message = `${label} no puede tener mas de ${err.local.limit} caracteres!`;
        break;
      case 'number.base':
        message = `${label} solo puede contener números!`;
        break;
      default:
        break;
    }
    // eslint-disable-next-line no-param-reassign
    err.message = message;
  });
  return errors;
};
const validate = (schema, model, separator = '<br />') => {
  const validation = schema.validate(model, { abortEarly: false });
  let formatedError;
  if (validation.error) {
    formatedError = [...validation.error.details.map((e) => [e.message])].join(separator);
    // formatedError = validation.error.details
    //   .reduce((concat, { message }) => [...concat, message], [])
    //   .join('<br />');
  }
  return { value: validation.value, error: validation.error, formatedError };
};
export { joiErrors, validate };
