import { createPool } from 'mysql2/promise';

let pool;
export default function getPool() {
  if (pool) {
    return pool;
  }
  const config = {
    connectionLimit: 100,
    host: 'localhost',
    user: 'root',
    password: 'lani0363',
    database: 'hotel',
    debug: false,
    waitForConnections: true,
    multipleStatements: true,
  };
  pool = createPool(config);
  return pool;
};
/*
connectionLimit: 100,
    host: 'localhost',
    user: 'root',
    password: 'lani0363',
    database: 'iae-nuevo',
    debug: false,
    waitForConnections: true,
    multipleStatements: true,
*/
