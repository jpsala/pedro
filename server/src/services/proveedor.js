/* eslint-disable one-var-declaration-per-line */
/* eslint-disable one-var */
/* eslint-disable newline-per-chained-call */
/* eslint-disable camelcase */
/* eslint-disable import/prefer-default-export */
import db from '../db';
import dayjs from './dayjs';
import { joiErrors, validate } from './joiValidate';

const Joi = require('@hapi/joi')
  .extend(require('@hapi/joi-date'));


const getProveedor = async (id) => {
  const [rows] = await db().query(`
    SELECT id, nombre, cuit, retencion, DATE_FORMAT(retencion_fecha, '%d/%m/%Y') as retencion_fecha, retencion_detalle
      from proveedor where id = ${id}
  `);
  return rows.length > 0 && rows[0];
};
const getProveedores = async () => {
  const [rows] = await db().query(`
    SELECT id, nombre, cuit, retencion, DATE_FORMAT(retencion_fecha, '%d/%m/%Y') as retencion_fecha, retencion_detalle
      from proveedor
  `);
  return rows;
};
const saveProveedorAlicuota = async (alicuota) => {
  const fechaRaw = new Date();
  const fecha = `${fechaRaw.getFullYear()}/${fechaRaw.getMonth()}/${fechaRaw.getDate()}`;
  const retencion = alicuota.retencion.retencion || 0;
  const error = (alicuota.retencion.error || '').replace('null', '');
  await db().query(`
    update proveedor
      set retencion = ${retencion},
      retencion_detalle = '${error}',
      retencion_fecha = '${fecha}'
    where id = ${alicuota.id}
  `);
};
const saveProveedor = async (proveedor) => {
  const schema = Joi.object({
    id: Joi.number(),
    retencion_detalle: Joi.string().min(4).optional().allow(null).allow('').empty('').default('').error(joiErrors),
    nombre: Joi.string().required().min(4).error(joiErrors),
    retencion_fecha: Joi.date().format(['DD/MM/YYYY', 'D/MM/YYYY'])
      .default(dayjs().format('DD/MM/YYY')).error(joiErrors),
    cuit: Joi.string().required().min(13).error(joiErrors),
    retencion: Joi.number().allow(0).required().error(joiErrors),
  });
  const validation = validate(schema, proveedor);
  if (validation.error) {
    return { error: validation.formatedError };
  }
  const proveedorValidado = Object.assign(validation.value, { retencion_fecha: proveedor.retencion_fecha });
  let rows, data, error;
  try {
    const { cuit, id, nombre, retencion, retencion_detalle, retencion_fecha } = proveedorValidado;
    let fecha;
    if (!retencion_fecha) fecha = null;
    else fecha = dayjs(retencion_fecha).format('YYYY/MM/DD');
    if (proveedorValidado.id === -1) {
      [rows] = await db().query(`
        insert into proveedor(nombre,cuit,retencion,retencion_detalle,retencion_fecha)
                    values(?,?,?,?,?)
      `, [nombre, cuit, retencion, retencion_detalle, fecha]);
    } else {
      [rows] = await db().query(`
      update proveedor
      set cuit = ?, nombre = ?, retencion = ?, retencion_detalle = ?, retencion_fecha = ?
      where id = ?`,
      [cuit, nombre, retencion, retencion_detalle, fecha, id]);
    }
    // eslint-disable-next-line prefer-destructuring
    data = rows;
  } catch (_error) {
    console.dir(_error);
    error = _error;
  }
  return { data, error };
};
export { getProveedores, saveProveedor, saveProveedorAlicuota, getProveedor };
