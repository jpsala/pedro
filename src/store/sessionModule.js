import Vue from 'vue'
import { parseToken, verifyToken } from '../services/token'
let logoutTimer
const moduleState = () => {
  return {
    token: undefined,
    tokenData: undefined,
    tokenSecondsForExpiration: '',
    user: undefined
  }
}

const moduleGetters = {
  loggedIn (state) {
    return Boolean(state.user)
  },
  expiration (state) {
    return state.tokenSecondsForExpiration || ''
  }
}

const moduleActions = {
  async initSession ({ dispatch, state }) {
    process.env.NODE_ENV === 'development' && console.log('initSession')
    const token = window.localStorage.getItem('pedro')
    const { error } = verifyToken(token)
    if (error) {
      console.log('error en initSession parsing token', error)
      await dispatch('logout')
      return
    }
    await dispatch('setToken', token)
    await dispatch('setStateFromToken')
  },
  async login (context, user) {
    console.log('login init')
    context.dispatch('addLoading', { id: 'login' }, { root: true })
    const ret = await Vue.prototype.$axios.post('login', user)
      .then(async (response) => {
        console.log('login axios response', response)
        context.dispatch('removeLoading', 'login', { root: true })
        if (!response) throw Error('Error de conexión')
        return response
      })
      .catch(async (error) => {
        console.log('erroooor', await error)
        throw error
      })
    return ret
  },
  async logout ({ dispatch }) {
    console.log('session logout')
    await dispatch('setToken', undefined)
    await dispatch('setUserData', undefined)
    await dispatch('setTokenData', undefined)
    await dispatch('setTokenSecondsForExpiration', 0)
    logoutTimer && clearTimeout(logoutTimer)
  },
  setToken ({ commit }, value) {
    commit('API_TOKEN', value)
  },
  setUserData ({ commit, dispatch }, data) {
    commit('USER', data)
  },
  setTokenData ({ commit }, data) {
    commit('TOKEN_DATA', data)
  },
  async setStateFromToken ({ dispatch, state }) {
    const token = state.token
    const payload = parseToken(token)
    if (!payload || !payload.user || !payload.exp) dispatch('logout')
    else {
      await dispatch('setUserData', payload.user)
      await dispatch('setTokenData', payload)
      await dispatch('setLogoutTimer', payload.exp)
    }
  },
  async setTokenFromHeader ({ dispatch }, token) {
    const { error } = verifyToken(token)
    if (error) {
      console.log('error en initSession parsing token', error)
      await dispatch('logout')
      return
    }
    await dispatch('setToken', token)
    await dispatch('setStateFromToken')
  },
  async refreshToken ({ dispatch }) {
    const resp = await Vue.prototype.$axios.get('token')
    const { error } = verifyToken(resp.data)
    if (error) {
      console.log('error en initSession parsing token', error)
      await dispatch('logout')
    }
    // await dispatch('setToken', token)
    // await dispatch('setStateFromToken')
  },
  setTokenSecondsForExpiration ({ commit }, seconds) {
    commit('tokenSecondsForExpiration', seconds)
  },
  async setLogoutTimer ({ dispatch, state, getters }) {
    logoutTimer && clearTimeout(logoutTimer)
    logoutTimer = setInterval(async () => {
      const seconds = Math.floor(state.tokenData.exp - (new Date().getTime() + 1) / 1000)
      const loggedIn = getters['loggedIn']
      if (!loggedIn || seconds <= 0) {
        console.warn('setLogoutTimer, user %O - seconds form expiration %O', loggedIn, seconds)
        if (loggedIn) dispatch('logout')
        clearTimeout(logoutTimer)
        return
      }
      dispatch('setTokenSecondsForExpiration', seconds)
    }, 1000)
  }
}

const moduleMutations = {
  USER (state, data) {
    Vue.set(state, 'user', data)
  },
  TOKEN_DATA (state, data) {
    state.tokenData = data
  },
  tokenSecondsForExpiration (state, data) {
    state.tokenSecondsForExpiration = data
  },
  API_TOKEN (state, value) {
    window.localStorage.setItem('pedro', value)
    state.token = value
  }

}
export default {
  namespaced: true,
  getters: moduleGetters,
  mutations: moduleMutations,
  actions: moduleActions,
  state: moduleState
}
