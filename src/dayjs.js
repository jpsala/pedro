import dayjs from 'dayjs'
import es from 'dayjs/locale/es'
import customParseFormat from 'dayjs/plugin/customParseFormat'
dayjs.locale('es', es)
dayjs.extend(customParseFormat)
export default dayjs
