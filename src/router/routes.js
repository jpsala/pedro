// import store from 'src/store'
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') },
      { path: '/login', component: () => import('pages/Login.vue') },
      { path: '/doc/:tipo/:id?', component: () => import('pages/Doc.vue') },
      { path: '/proveedorAdmin', component: () => import('pages/ProveedorAdmin.vue') },
      { path: '/proveedor/:id', component: () => import('pages/ProveedorForm.vue') },
      { path: '/retenciones', component: () => import('pages/Retenciones.vue') }

    ]
  },
  {
    path: '/print',
    component: () => import('layouts/PrintLayout.vue'),
    children: [
      { path: '', component: () => import('pages/print/Doc.vue') },
      { path: 'doc/:id', component: () => import('pages/print/Doc.vue') }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
