import { Dialog, Notify } from 'quasar'
import { watch } from '@vue/composition-api'
import store from 'src/store'

const alerta = async (title = 'Alerta', message = '', html = false) => {
  return new Promise((resolve, reject) => {
    Dialog.create({
      title,
      message,
      html
    }).onOk(() => {
      resolve()
    })
  })
}
const alertaError = async (title = 'Error', message = '', html = false) => {
  return new Promise((resolve, reject) => {
    Dialog.create({
      class: 'bg-red-2',
      title,
      message,
      html
    }).onOk(() => {
      resolve()
    })
  })
}
const notify = ({ message = 'Falta', error = false, timeout = 3000, icon = '' } = {}) => {
  console.log('message', message)
  Notify.create({
    color: error ? 'red-4' : 'green-4',
    textColor: 'white',
    icon,
    message: message,
    timeout: timeout
  })
}
const soloEnDevMode = func => {
  if (process.env.NODE_ENV === 'development') func()
}
const log = (...args) => {
  if (!process.env.NODE_ENV === 'development') return
  console.warn(...args)
  console.trace()
}
const handleAxiosError = async (error) => {
  const method = error.config.method
  const endpoint = method + ' ' + error.config.url.replace(/^[a-z]{4,5}:\/{2}[a-z]{1,}:[0-9]{1,4}.(.*)/, '$1') // http or https
  const statusCode = error.response && error.response.status
  let msg = 'no asignado'
  console.log('error', error)
  if (error.response && error.response.data) msg = error.response.data
  else if (typeof (error.toJSON) === 'function') {
    const json = error.toJSON()
    msg = json && json.message
  }
  if (typeof (msg) === 'object') msg = JSON.stringify(msg)
  if (statusCode === 404) {
    await alertaError(`Error en ${endpoint}`, 'La página que busca no fué encontrada')
  } else if (statusCode === 401) {
    await alertaError(`Error en ${endpoint}`, 'Página protegida. Use la página de ingreso para poder acceder!')
  } else if (msg) {
    await alertaError(`Error en ${endpoint}`, `${msg}`)
  } else {
    await alertaError(`Error en ${endpoint}`, `Error Desconocido, ver la consola``Error en ${endpoint}`)
  }
  return error
}
const elIsVisible = (domElement) => {
  return new Promise(resolve => {
    const o = new IntersectionObserver(([entry]) => {
      resolve(entry.intersectionRatio === 1)
      o.disconnect()
    })
    o.observe(domElement)
  })
}
const onLoggedIn = async () => {
  return new Promise((resolve) => {
    watch(() => store.getters['session/loggedIn'], async (loggedIn) => {
      if (loggedIn) resolve()
    })
  })
}
export { notify, soloEnDevMode, log, handleAxiosError, elIsVisible, onLoggedIn, alerta, alertaError }
