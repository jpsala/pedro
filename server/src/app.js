/* eslint-disable guard-for-in */
/* eslint-disable no-restricted-syntax */
import express from 'express';
import dotenv from 'dotenv';

const app = express();
const history = require('connect-history-api-fallback');

dotenv.config();

const staticPath = process.env.STATIC;
if (!staticPath) throw new Error('set the static path in .env');
const staticFileMiddleware = express.static(staticPath);
app.use(staticFileMiddleware);
app.use(history({
  index: '/index.html',
}));
app.use(staticFileMiddleware);

const port = process.env.EXPRESS_PORT || 8080;
app.listen(port, () => {
  console.log(`Listening on port ${port} staticPath ${staticPath} modo ${process.env.NODE_ENV}`);
});
