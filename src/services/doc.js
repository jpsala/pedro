import Vue from 'vue'
const getDoc = async (id) => {
  const resp = await Vue.prototype.$axios.get(`doc/${id}`)
  return resp.data
}
const createDoc = async (doc) => {
  return Vue.prototype.$axios.post('doc', doc, { timeout: 3000 })
}

export { getDoc, createDoc }
