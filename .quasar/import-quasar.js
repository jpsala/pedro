/**
 * THIS FILE IS GENERATED AUTOMATICALLY.
 * DO NOT EDIT.
 *
 * You are probably looking on adding startup/initialization code.
 * Use "quasar new boot <name>" and add it there.
 * One boot file per concern. Then reference the file(s) in quasar.conf.js > boot:
 * boot: ['file', ...] // do not add ".js" extension to it.
 *
 * Boot files are your "main.js"
 **/

import lang from 'quasar/lang/es'


import Vue from 'vue'

import {Quasar,QLayout,QHeader,QDrawer,QDate,QPopupProxy,QPageContainer,QPage,QToolbar,QToolbarTitle,QBtn,QIcon,QList,QItem,QItemSection,QItemLabel,QAvatar,QFooter,QCard,QCardSection,QExpansionItem,QCardActions,QForm,QInput,QToggle,QBtnToggle,QSeparator,QImg,QSpinner,QBanner,QTooltip,QSpinnerTail,QInnerLoading,QAjaxBar,QTable,QTh,QTr,QTd,QSelect,QBtnDropdown,QMarkupTable,QMenu,QDialog,QPageSticky,QFab,QFabAction,QScrollArea,QField,QSpace,Ripple,ClosePopup,Notify,Dialog} from 'quasar'


Vue.use(Quasar, { config: {"brand":{"primary":"#009688"}},lang: lang,components: {QLayout,QHeader,QDrawer,QDate,QPopupProxy,QPageContainer,QPage,QToolbar,QToolbarTitle,QBtn,QIcon,QList,QItem,QItemSection,QItemLabel,QAvatar,QFooter,QCard,QCardSection,QExpansionItem,QCardActions,QForm,QInput,QToggle,QBtnToggle,QSeparator,QImg,QSpinner,QBanner,QTooltip,QSpinnerTail,QInnerLoading,QAjaxBar,QTable,QTh,QTr,QTd,QSelect,QBtnDropdown,QMarkupTable,QMenu,QDialog,QPageSticky,QFab,QFabAction,QScrollArea,QField,QSpace},directives: {Ripple,ClosePopup},plugins: {Notify,Dialog} })
