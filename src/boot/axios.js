import axios from 'axios'
import { handleAxiosError } from '../helpers'

// const locals = ['localhost']
// const hostname = document.location.hostname
// const local = locals.includes(hostname)
// const isServer = hostname === '192.168.2.254'
// const pcJP = window.localStorage.getItem('pcJP')
export default async ({ Vue, store }) => {
  console.log('document.location.hostname', document.location.hostname)
  axios.defaults.baseURL = document.location.hostname === 'hotel.iae.com.ar'
    ? 'https://hotel.iae.com.ar:3000/api/hotel/'
    : 'http://localhost:8888/api/hotel/'
  axios.interceptors.response.use((response) => {
    const endPoint = response.config.url
    console.log('endpoint %O response %O', endPoint, response)
    if (response.headers.token) {
      console.log('response.headers.token', response.headers.token ? response.headers.token.substr(0, 10) : 'no hay token')
      store.dispatch('session/setTokenFromHeader', response.headers.token)
    }
    return response
  }, (error) => {
    const endPoint = error.config.url
    console.error('endpoint %O response %O', endPoint, error)
    return Promise.reject(handleAxiosError(error))
  })

  axios.interceptors.request.use(
    async (config) => {
      config.headers.token = store.state.session.token
      return config
    },
    error => Promise.reject(error)
  )

  axios.defaults.method = 'POST'
  // axios.defaults.validateStatus = function (status) {
  //   return status >= 200 && status <= 503
  // }
  Vue.prototype.$axiosRaw = axios.create()
  Vue.prototype.$axios = axios
}
