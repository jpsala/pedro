import Vue from 'vue'
import { Promise } from 'core-js'
const deleteProveedor = async (id) => {
  try {
    const resp = await Vue.prototype.$axios.delete(`/proveedor/${id}`)
    return resp
  } catch (error) {
    throw error
  }
}
const getProveedores = () => {
  return Vue.prototype.$axios.get('/proveedor', {
  }).then((resp) => {
    return resp.data
  })
}
const getProveedor = async (id) => {
  return Vue.prototype.$axios.get(`/proveedor/${id}`)
}
const getRetencion = async (proveedor) => {
  return new Promise((resolve) => {
    return Vue.prototype.$axios({
      method: 'get',
      url: `/proveedor/${proveedor.cuit}/retencion`,
      timeout: 400000
    }).then((resp, b) => {
      return resolve(resp.data || { retencion: { error: 'Error en la conexión con ARBA, tiempo de espera terminado, vuelva a intentar luego' } })
    }).catch(e => {
      console.log('error', e)
      return e
    })
  })
}
const saveProveedor = async (proveedor) => {
  return Vue.prototype.$axios.post('/proveedor', proveedor)
    .then((resp) => resp)
    .catch(e => {
      throw (e)
    })
}
export { getProveedores, saveProveedor, getRetencion, getProveedor, deleteProveedor }
