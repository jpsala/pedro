// Configuration for your app
// https://quasar.dev/quasar-cli/quasar-conf-js
const CopyWebpackPlugin = require('copy-webpack-plugin')
module.exports = function (ctx) {
  return {
    // app boot file (/src/boot)
    // --> boot files are part of "main.js"
    boot: [
      'axios',
      'icomoon',
      'vue-validate'
    ],

    css: [
      'app.styl'
    ],

    extras: [
      // 'ionicons-v4',
      // 'mdi-v3',
      // 'fontawesome-v5',
      // 'eva-icons',
      // 'themify',
      // 'roboto-font-latin-ext', // this or either 'roboto-font', NEVER both!

      'roboto-font', // optional, you are not bound to it
      'material-icons' // optional, you are not bound to it
    ],

    framework: {
      // iconSet: 'ionicons-v4',
      lang: 'es', // Quasar language

      // all: true, // --- includes everything; for dev only!
      config: {
        brand: {
          primary: '#009688'
        }
      },
      components: [
        'QLayout',
        'QHeader',
        'QDrawer',
        'QDate',
        'QPopupProxy',
        'QPageContainer',
        'QPage',
        'QToolbar',
        'QToolbarTitle',
        'QBtn',
        'QIcon',
        'QList',
        'QItem',
        'QItemSection',
        'QItemLabel',
        'QAvatar',
        'QFooter',
        'QCard',
        'QCardSection',
        'QExpansionItem',
        'QCard',
        'QCardSection',
        'QCardActions',
        'QForm',
        'QInput',
        'QToggle',
        'QBtnToggle',
        'QSeparator',
        'QImg',
        'QSpinner',
        'QBanner',
        'QTooltip',
        'QSpinnerTail',
        // 'QSpinnerPuff',
        'QInnerLoading',
        'QAjaxBar',
        'QTable',
        'QTh',
        'QTr',
        'QTd',
        'QSelect',
        'QBtnDropdown',
        'QMarkupTable',
        'QMenu',
        'QDialog',
        'QPageSticky',
        'QFab',
        'QFabAction',
        'QScrollArea',
        'QField',
        'QSpace'
      ],

      directives: [
        'Ripple',
        'ClosePopup'
      ],

      // Quasar plugins
      plugins: [
        'Notify',
        'Dialog'
      ]
    },

    supportIE: false,

    build: {
      scopeHoisting: true,
      vueRouterMode: 'history',
      // vueCompiler: true,
      gzip: true,
      // publicPath: '/buffet2',
      // publicPath: '',
      // analyze: true,
      devtool: 'source-map',
      extractCSS: true,
      extendWebpack (cfg) {
        cfg.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /node_modules/,
          options: {
            formatter: require('eslint').CLIEngine.getFormatter('stylish')
          }
        })
        cfg.plugins.push(
          new CopyWebpackPlugin(
            [{ context: `${__dirname}/src/root`, from: '*.*' },
              { context: `${__dirname}/src/root`, from: '.*' }]
          )
        )
      }

    },

    devServer: {
      // https: true,
      // port: 8080,
      // open: 'google-chrome-stable-debug'
      // open: true // opens browser window automatically
    },

    // animations: 'all', // --- includes all animations
    animations: 'all',

    ssr: {
      pwa: false
    },

    pwa: {
      // workboxPluginMode: 'InjectManifest',
      // workboxOptions: {}, // only for NON InjectManifest
      manifest: {
        // name: 'Buffet',
        // short_name: 'Buffet',
        // description: 'App Buffet del IAE',
        display: 'standalone',
        orientation: 'portrait',
        background_color: '#ffffff',
        theme_color: '#027be3',
        icons: [
          {
            'src': 'statics/icons/icon-128x128.png',
            'sizes': '128x128',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-192x192.png',
            'sizes': '192x192',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-256x256.png',
            'sizes': '256x256',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-384x384.png',
            'sizes': '384x384',
            'type': 'image/png'
          },
          {
            'src': 'statics/icons/icon-512x512.png',
            'sizes': '512x512',
            'type': 'image/png'
          }
        ]
      }
    },

    cordova: {
      // id: 'com.iae.buffet.app',
      // noIosLegacyBuildFlag: true, // uncomment only if you know what you are doing
    },

    electron: {
      // bundler: 'builder', // or 'packager'

      extendWebpack (cfg) {
        // do something with Electron main process Webpack cfg
        // chainWebpack also available besides this extendWebpack
      },

      packager: {
        // https://github.com/electron-userland/electron-packager/blob/master/docs/api.md#options

        // OS X / Mac App Store
        // appBundleId: '',
        // appCategoryType: '',
        // osxSign: '',
        // protocol: 'myapp://path',

        // Windows only
        // win32metadata: { ... }
      },

      builder: {
        // https://www.electron.build/configuration/configuration

        // appId: 'buffet'
      }
    }
  }
}
