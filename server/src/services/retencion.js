import axios from './axios';
import db from '../db';

const getRetenciones = async (desde, hasta) => {
  const [rows] = await db().query(`
    SELECT d.id, d.numero, p.nombre as proveedor, p.cuit, p.retencion as alicuota, DATE_FORMAT(d.fecha, '%d/%m/%Y') AS fecha, d.total, d.retencion
    FROM doc d
      INNER JOIN proveedor p ON d.proveedor_id = p.id
    WHERE d.fecha BETWEEN '${desde}' AND '${hasta}' AND d.tipo = 'op'
`);
  return rows;
};
const getRetencion = async (cuit) => {
  let retencion = undefined
  try {
    //http://iae.dyndns.org/iae/index.php?r=apiApp/
    retencion = await axios.post('getAlicuota', { cuit: cuit.replace(/-/g, '') });
    return retencion.data;
  } catch (error) {
    console.log('error obteniendo alicuota')
    console.dir(error)
  }
  return undefined
};

export { getRetenciones, getRetencion };
